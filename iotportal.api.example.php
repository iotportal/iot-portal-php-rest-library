<?php
/**
 * REST API Code Sample
 * 
 * @copyright   Copyright (c) 2015 Iotportal.io
 * @author      Jonathan <jonathan@iotportal.io>
 */

include 'iotportal.api.lib.php';

/************************************************
  ATTENTION: Fill in these values! Make sure
  version : v0
  key: '<YOUR_API_KEY>'; go to http://app.iotportal.io/api
 ************************************************/
$api_handler = new Iotportal(array('version'=>'v0', 'key'=>'<YOUR_API_KEY>'));

/************************************************
  READ MESSAGE REQUEST
  topic: name of topic, must contain mqtt username
  broker: the id of broker (the broker id is shown on the brokers page in the green box (first column of the table))
  json format response
************************************************/
$read_param = array(	
					'topic'		=>'<YOUR_MQTT_USER>/<MQTT_TOPIC>', 
					'broker'	=>1
				);
$msg_resp = $api_handler->read_handler($read_param);
$msg_resp = json_decode($msg_resp);

echo '<h2>Get Message content</h2>';
if($msg_resp->error == 0){
	echo '<table class="reference" style="width:100%; ">
		<tbody><tr>
			<th>Id</th>
			<th>Broker</th>
			<th>Topic</th>		
			<th>Data</th>		
			<th>LoggerID</th>		
			<th>Created (nanosecond)</th>		
			<th>Mqtt Owner Id</th>
			<th>Logger Topic Id</th>
		</tr>';

	foreach ($msg_resp->data as $key => $message) {
		echo 	'<tr style="border: 1px solid #ddd;">
					<td>'.$message->Id.'</td>
					<td>'.$message->Broker.'</td>
					<td>'.$message->Topic.'</td>
					<td>'.$message->Data.'</td>
					<td>'.$message->LoggerID.'</td>
					<td>'.$message->Created.'</td>
					<td>'.$message->Mqtt_owner_id.'</td>
					<td>'.$message->Logger_topic_id.'</td>
				</tr>';
	}	

	echo '</tbody></table>';
}else{
	echo "There are no message!";
}

/************************************************
  PUBLISH MESSAGE REQUEST
  topic: name of topic, must content mqtt username
  broker: the id of broker (the broker id is shown on the brokers page in the green box (first column of the table))
  message: content to publish
  json format response
************************************************/
$publish_param = array(	'topic'		=>'<YOUR_MQTT_USER>/<MQTT_TOPIC>', 
						'broker'	=>1,
						'message'	=>'IoTPortal is the best'
					);

$publish_result = $api_handler->publish_handler($publish_param);
$publish_result = json_decode($publish_result);
echo '<h2>Publish Message</h2>';
if($publish_result->error == 0){
	//success case
	echo $publish_result->message;
}else{
	//error case
	echo $publish_result->message;
}

/************************************************
  DELETE MESSAGE REQUEST
  topic: name of topic, must contain mqtt username
  broker: the id of broker (the broker id is shown on the brokers page in the green box (first column of the table))
  json format response
************************************************/
$delete_param = array(	'topic'		=>'<YOUR_MQTT_USER>/<MQTT_TOPIC>', 
						'broker'	=>1
					);
$delete_result = $api_handler->delete_handler($delete_param);
$delete_result = json_decode($delete_result);
echo '<h2>Delete Logged Topic</h2>';
if($delete_result->error == 0){
	//success case
	echo $delete_result->message;
}else{
	//error case
	echo $delete_result->message;
}

?>




