<?php

define( 'IOTPORTAL_API_ENDPOINT', 'http://api.iotportal.io/' );
define( 'IOTPORTAL_METHOD_POST', 'post' );
define( 'IOTPORTAL_METHOD_GET', 'get' );
define( 'IOTPORTAL_METHOD_DELETE', 'delete' );

/**
 * Library to access the iotportal services
 * 
 * @copyright   Copyright (c) 2015 Iotportal.io
 * @author      Jonathan <jonathan@iotportal.io>
 */

class Iotportal {
	/**
	 * Holder for the initial configuration parameters 
	 * 
	 * @var     resource
	 * @access  private
	 */
	private $apikey = NULL;
	private $version = NULL;
	
	/**
	 * Constructor method
	 * 
	 * @param  array         Configuration parameters for the library
	 */
	public function __construct( $param) {

		// Store the config values
		if(!$param){
			die('please set your api key');
		}

		$this->apikey = $param['key'];
		$this->version = $param['version'];
	}

    /**
     * send message
     *
     * @param  array        Customer rguments
     */
    public function publish_handler( $params = null ) {
        if (!$params)
            $params = array();
        return $this->_send_request( 'publish', $params, IOTPORTAL_METHOD_POST );
    }

   	/**
	 * Get messages
	 * 
	 * @param  string        Customer ID to retrieve messages only for a given customer
	 * @param  int           Number of messages to retrieve, default 10, max 100
	 * @param  int           Offset to start the list from, default 0
	 */
	public function read_handler($params = null ) {
	   	if (!$params)
            $params = array();
		return $this->_send_request( 'data', $params, IOTPORTAL_METHOD_POST);
	}

	/**
	 * delete messages
	 * 
	 * @param  string        Customer ID to retrieve messages only for a given customer
	 * @param  int           Number of messages to retrieve, default 10, max 100
	 * @param  int           Offset to start the list from, default 0
	 */
	public function delete_handler($params = null ) {
	   	if (!$params)
            $params = array();
		return $this->_send_request( 'delete', $params, IOTPORTAL_METHOD_POST );
	}

	/**
	 * Private utility function that prepare and send the request to the API servers
	 * 
	 * @param  string        The URL segments to use to complete the http request
	 * @param  array         The parameters for the request, if any
	 * @param  srting        Either 'post','get' or 'delete' to determine the request method, 'get' is default
	 */
	private function _send_request( $url_segs , $params = array(), $http_method = 'get' ) {
		// Initializ and configure the request
		$req = curl_init(IOTPORTAL_API_ENDPOINT.$this->apikey.'/'.$this->version .'/mqtt/'.$url_segs);
		//http://api.iotportal.io/tungtt/v0/mqtt/data
		curl_setopt( $req, CURLOPT_HTTPAUTH, CURLAUTH_ANY );
		curl_setopt( $req, CURLOPT_RETURNTRANSFER, TRUE );

		// Are we using POST? Adjust the request properly
		if( $http_method == IOTPORTAL_METHOD_POST ) {
			curl_setopt( $req, CURLOPT_POST, TRUE );
			curl_setopt( $req, CURLOPT_POSTFIELDS, http_build_query( $params, NULL, '&' ) );
		}
		
		// if( $http_method == IOTPORTAL_METHOD_DELETE ) {
		// 	curl_setopt( $req, CURLOPT_POST, TRUE );
		// 	curl_setopt( $req, CURLOPT_POSTFIELDS, http_build_query( $params, NULL, '&' ) );
		// }
		
		// Get the response, clean the request and return the data
		$response = curl_exec( $req );
		curl_close( $req );
		return $response;
	}
}
// END Iotportal Class
/* End of file iotportal_api.php */

